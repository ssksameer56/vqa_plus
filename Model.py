import os
import io
import requests
from PIL import Image
import pandas as pd

import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torchvision import datasets, models, transforms
from torch.utils.data import Dataset, DataLoader
from torch.autograd import Variable

import gensim
from gensim.test.utils import datapath, get_tmpfile
from gensim.models import KeyedVectors
from gensim.scripts.glove2word2vec import glove2word2vec

import VideoUtils
import TextUtils
from TextUtils import GloVE
#device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
#torch.set_printoptions(threshold=5000)

class Decoder(nn.Module):
  def __init__(self, input_dim, hidden_dim, batch_size = 1, output_dim=1,
                    num_layers=1):
    super(Decoder, self).__init__()
    self.input_dim = input_dim
    self.hidden_dim = hidden_dim
    self.batch_size = batch_size
    self.num_layers = num_layers
    self.hidden = None
    # Define the LSTM layer
    self.lstm = nn.LSTM(self.input_dim, self.hidden_dim, self.num_layers)

  def init_hidden(self):
    # This is what we'll initialise our hidden state as
    return (torch.zeros(self.num_layers, self.batch_size, self.hidden_dim),
            torch.zeros(self.num_layers, self.batch_size, self.hidden_dim))

  def forward(self, input):
    lstm_out, self.hidden = self.lstm(input, self.hidden)
    return lstm_out, self.hidden

class FCUniClassifier(nn.Module):
  """Fully Conntected Classifier that combines image and question"""
  def __init__(self, vocab_length):
    """Initialise the layers in the model"""
    super(FCUniClassifier, self).__init__()
    self.fc_first = nn.Linear(1024,1000)
    self.dropout_1 = nn.Dropout(p=0.5)
    self.fc_second = nn.Linear(1000,vocab_length)
    self.dropout_2 = nn.Dropout(p=0.5)
    self.tanh = nn.Tanh()
  def forward(self, img_que):
    """Defines a forward pass in the model"""
    output_1 = self.dropout_1(img_que)
    output_2 = self.fc_first(output_1)
    output_3 = self.dropout_1(output_2)
    output_4 = self.fc_second(output_3)
    output = self.tanh(output_4)
    return output

class VQA(nn.Module):
  """ Class with the VQA Model"""
  def __init__(self, img_emb_size, que_emb_size, vocab_length, hidden_size,
              bidirectional, num_layers):
    """Initlialise all the layers in model and their sequence"""
    super(VQA, self).__init__()
    self.img_emb_size = img_emb_size
    self.que_emb_size = que_emb_size
    self.hidden_size = hidden_size
    self.bidirectional = bidirectional
    self.num_layers = num_layers
    #Image    
    self.fc_image = nn.Linear(self.img_emb_size, 1024, True)
    #Question
    self.hidden = None
    self.init_hidden()
    self.lstm = nn.LSTM(self.que_emb_size,self.hidden_size, 
                        num_layers = self.num_layers,
                        bidirectional = self.bidirectional)
    self.q_cat_size = self.num_layers * self.hidden_size *\
                      (2 if self.bidirectional else 1)
    self.fc_question = nn.Linear(self.q_cat_size,1024, True)
    #Unified
    self.fc_unified_classifier = FCUniClassifier(vocab_length)
    self.tanh = nn.Tanh()

  def init_hidden(self):
    """Initialise the hidden layer to zero"""
    layers = self.num_layers*(2 if self.bidirectional else 1)
    self.hidden = (torch.zeros(layers, 1, self.hidden_size), 
                   torch.zeros(layers, 1, self.hidden_size))
  
  def forward(self, frames, question):
    """Defines how a forward pass happens"""
#    frame_softmax_outputs = []
    question = question.unsqueeze(1) #Add dimension for batch size = 1
    _, self.hidden = self.lstm(question, self.hidden) #Process the question tokens
    que_cat = self.hidden[0].view(1,2048)
    #que_cat is the concatenated version of hidden states to generate (1,2048) vector
    fc_que_output = self.fc_question(que_cat)
    fc_img_output = self.fc_image(frames)
    fc_img_output = self.tanh(fc_img_output)
    #multiply que output to frames (rudimentary attention)
    elem_mul = fc_img_output * fc_que_output
    fc_unified = self.fc_unified_classifier(elem_mul)
    vqa_representation = torch.mean(fc_unified, dim = 0)
    #Each row is frame, each column is word
    return vqa_representation