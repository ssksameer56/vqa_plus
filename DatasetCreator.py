import os
import io
import requests
from PIL import Image
import pandas as pd
import spacy
import cv2
import numpy as np
import csv
from nltk.tokenize import RegexpTokenizer
import pickle

import keras
from keras.applications.vgg16 import VGG16, preprocess_input

import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torchvision import datasets, models, transforms, utils
from torch.utils.data import Dataset, DataLoader
from torch.autograd import Variable

import gensim
from gensim.test.utils import datapath, get_tmpfile
from gensim.models import KeyedVectors
from gensim.scripts.glove2word2vec import glove2word2vec

import scipy.io

class VideoEmbeds():
  def __init__(self):
    model = VGG16(weights='imagenet', include_top = True, input_shape=(224,224,3))
    self.m = keras.models.Model(inputs=model.input, outputs=[model.layers[-3].output])
    
  def preprocess_img(self, video_name):
    video = cv2.VideoCapture(video_name)
    frame_list = []
    frame_num = 0
    while True:
      try:
        okay, frame = video.read()
        if not okay:
          #print("frame not read", frame_num)
          break
        frame = cv2.resize(frame, (224, 224))
        frame = preprocess_input(frame)
        feat_vec = self.m.predict(frame.reshape(1, *frame.shape))
        print(feat_vec)
        frame_list.append(feat_vec)
        frame_num += 1
      except KeyboardInterrupt:  # press ^C to quit
        break
    #concat the frames
    dump = np.empty((1, len(frame_list), 4096))
    for i in range(len(frame_list)):
      dump[0,i,] = frame_list[i]
    print(dump, frame_num)
    return dump

class GloVE():
  """Class that generates a GloVE Embedding for any word provided"""
  def __init__(self, glove_path):
    """Initialise the embedding and model from txt file"""
    #glove_file = datapath(os.path.abspath('./glove.840B.300d.txt'))
    #tmp_file = get_tmpfile(os.path.abspath("./glove_word2vec.txt"))
    #_ = glove2word2vec(glove_file, tmp_file)
    self.model = gensim.models.KeyedVectors.load_word2vec_format(os.path.abspath(glove_path))
    self.weights = torch.FloatTensor(self.model.vectors)
    self.embedding = nn.Embedding.from_pretrained(self.weights)

  def create_embedding(self, question):
    """Create a list of embeddings from the token provided"""
    e_len = len(question)
    embeds = torch.Tensor(e_len,300)
    for x in range(e_len):
      try:
        input = torch.LongTensor([self.model.vocab[question[x]].index])
        embeds[x] = self.embedding(input)
      except KeyError:
        input = torch.LongTensor([self.model.vocab["unk"].index])
        embeds[x] = self.embedding(input)
    embeds = embeds.unsqueeze(1) #Make it LSTM Compatible. Tensor is now (Seq Size, Batch Size, Input Size)
    return embeds

class TextPreProcess():
  def __init__(self):
    """Initialise the spacy NLP Corpus"""
    self.nlp = spacy.load('en_core_web_sm')

  def tokenize(self, sentence):
    """Return tokens of the words and symbols in the sentence"""
    token_list = []
    doc = self.nlp(sentence)
    for token in doc:
        token_list.append(token.text)
    return token_list

class AnswerEmbeds():
  def __init__(self, vocab_path):
    fp = open(vocab_path, 'rb')
    self.vocab = list(pickle.load(fp))
    self.nltk_tokenizer = RegexpTokenizer(r'\w+')
    
  def find_index(self, word):
    return self.vocab.index(word)
  
  def tokenize(self, sentence):
    return self.nltk_tokenizer.tokenize(sentence.lower())
  
  def create_embedding(self, sentence):
    embeds = []
    for x in sentence:
      embeds.append(self.find_index(x))
    return embeds

'''Create dictionary in this form {visual: [visual encoding], ques:[q1, q2...], ans:[a1, a2...]}'''
def createDict(videopath, vqa_dict):
    dataset = {}
    dataset["ques"] = []
    dataset["ans"] = []
    for q in vqa_dict["ques"]:
        ques = t.tokenize(q)
        ques = g.create_embedding(ques)
        dataset["ques"].append(ques.numpy())
    for a in vqa_dict["ans"]:
        print(a)
        ans = amodel.tokenize(a)
        print(type(ans))
        ans.insert(0, '<SOS>')
        ans.append('<EOS>')
        print(ans)
        ans = amodel.create_embedding(ans)
        print('embedding', ans)
        dataset["ans"].append(np.asarray(ans))
    dataset["visual"] = v.preprocess_img(videopath)
    return dataset

def wholeDict(csv_reader):
    global_dict = {}
    for row in csv_reader:
        if row[0] in global_dict:
            global_dict[row[0]]["ques"].append(row[1])
            global_dict[row[0]]["ans"].append(row[2])
        else:
            global_dict[row[0]] = {}
            global_dict[row[0]]["ques"] = []
            global_dict[row[0]]["ques"].append(row[1])
            global_dict[row[0]]["ans"] = []
            global_dict[row[0]]["ans"].append(row[2])
    return global_dict

def dataset_creator(VideoIdFile, VideoFolder, CSVFile):
    csvfile = open(CSVFile)
    csv_reader = csv.reader(csvfile, delimiter = ',')
    global_dict = wholeDict(csv_reader)
    print(len(global_dict))
    
    videoids = open(VideoIdFile, "r")
    
    allVideoIds = videoids.readlines()

    for id in allVideoIds:
      id = id[:-1]
      try:
        print(id)
        print(global_dict[id])
        scipy.io.savemat(id + ".mat", createDict(VideoFolder+id+".gif", global_dict[id]))
      except KeyError:
        print("No QA for"+id)
        continue #If QA Pair not found for video skip
#The file should contain all the video ids.
if __name__ == "__main__":
  """
  CSVFile     - File with QA Pair
  VideoIdFile - File with Video IDs to be processed. Generated by dataset_list.py
  VideoFolder - Folder containing the renamed GIFs. Generated by renameGIF.py
  """ 
  CSVFile = '/content/drive/My Drive/B Tech./B.Tech Project/Dataset/QA.csv'
  VideoIdFile = "/content/drive/My Drive/B Tech./B.Tech Project/Dataset/GIF_R/videoid.txt" 
  VideoFolder = "/content/drive/My Drive/B Tech./B.Tech Project/Dataset/GIF_R/"
  glove_path = "/content/drive/My Drive/B Tech./B.Tech Project/glove_word2vec.txt"
  vocab_path = '/content/drive/My Drive/B Tech./B.Tech Project/Dataset/Vocabulary'
  g = GloVE(glove_path)
  amodel = AnswerEmbeds()
  t = TextPreProcess()
  v = VideoEmbeds()

  dataset_creator(VideoIdFile, VideoFolder, CSVFile)
  #mat = scipy.io.loadmat('0.mat')
  #for x in mat["ques"][0]:
  #  print(x)
  #for x in mat["ans"][0]:
  #  print(x)
