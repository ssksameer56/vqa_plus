#Code for model to act as demo
import io, requests, os, sys
from PIL import Image
import pandas as pd

import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torchvision import datasets, models, transforms
from torch.utils.data import Dataset, DataLoader
from torch.autograd import Variable

import Model, TextUtils, VideoUtils #Custom Modules for models and utilities

class VideoQADataset(Dataset):
  """Class that handles Dataset. Derived from inbuilt PyTorch Dataset"""
  def __init__(self, vid_path, data_file, transform):
    """ Initialises QA File, Video directory and PyTorch transforms
        vid_path:   Dir containing videos
        data_file:  Path to csv file containing name of videos, question and answer
                    transform
    """
    self.textpp = TextUtils.TextPreProcess()
    self.video_dir = vid_path
    self.data_file = pd.read_csv(data_file, sep='\t')
    self.channels = 3
    self.xSize = 224  #Size of VGG Input
    self.ySize = 224  #Size of VGG Input
    self.normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
    self.resize = transforms.Compose([transforms.ToPILImage(), 
                                      transforms.Resize((224,224)), 
                                      transforms.ToTensor(), self.normalize])
    self.transform = transform
  def __len__(self):
    return len(self.data_file)
  def __getitem__(self, idx):
    """Override default getitem code to retrieve specific item in dataset"""
    #Temp Code. Assuming some stuff here
    vid_name = os.path.join(os.abs.path(self.video_dir), self.data_file.iloc[idx, 0])
    frames = VideoUtils.ConvertVideoToFrames(vid_name)
    frames = self.Frames2Tensor(frames) #Apply transforms if necessary to frames
    QA_pair = self.data_file.iloc[idx, 1:].tolist()
    qa_tokens = self.QA2Tensor(QA_pair)
    return frames, qa_tokens
  
  def Frames2Tensor(self, frames):
    """
    Converts list of frames into tensors. Applies transforms to make it  VGG Compatible
    https://github.com/MohsenFayyaz89/PyTorch_Video_Dataset
    frames: numpy array containing the frames
    """
    timeDepth = len(frames)
    t_frames = torch.FloatTensor(self.channels, timeDepth, self.xSize, self.ySize)
    for f in range(timeDepth):
        #Error in processing last frame leads to NoneType Error
        frame = torch.from_numpy(frames[f]) #Convert the numpy to tensor
        if self.transform is not None:
            frame = self.resize(frame)  #Apply transforms to convert image if argument given
        t_frames[f, :, :, :] = frame #Tensor is now (index, channels, width, height)
    t_frames.unsqueeze(1) #Change tensor to (index, batch size, channels, width, height)
    t_frames  
    return t_frames

  def QA2Tensor(self, qa_pair):
    #Not Tested. Works on assumption
    """ Changes a question answer pair into tokens for further GloVE Processing
        qa_pair: list of question and answer    
    """
    que = qa_pair[0]
    ans = qa_pair[1]
    qa_list = []
    qa_list[0] = self.textpp.tokenize(que)
    qa_list[1] = self.textpp.tokenize(ans)
    return qa_list

def test(vid_dir, csv_file, transform, vid_num, mode, vocab_length):
    vqa_model = Model.VQA(vocab_length, "test")
    vqa_model.eval()
    video_qa_dataset = VideoQADataset(vid_dir, csv_file, transform = True)
    data = video_qa_dataset[vid_num]
    frames = data[0]
    question = data[1][0]
    answer_pred = vqa_model(frames, question)
    return(answer_pred, data[1][1])

if __name__ == "__main__":
    vid_num = sys.argv[3]
    vid_dir = sys.argv[1]
    csv_file = sys.argv[2]
    question = sys.argv[4]
    print(test(vid_dir, csv_file, True, vid_num, "test", 3000))