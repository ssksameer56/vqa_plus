import io, requests, os
from PIL import Image
import pandas as pd
import numpy as np

import pickle 

import scipy.io as scio

import torch
import torch.nn as nn
import torch.optim as optim
from torch.optim import lr_scheduler
from torchvision import datasets, models, transforms
from torch.utils.data import Dataset, DataLoader
from torch.autograd import Variable

import Model, TextUtils, VideoUtils #Custom Modules for models and utilities

class VideoQADataset(Dataset):
  """Class that handles Dataset. Derived from inbuilt PyTorch Dataset"""
  def __init__(self, vid_path):
    """ Initialises directory containing all the emebeddings
        vid_path:   Dir containing all embeddings
    """
    self.vid_dir = vid_path
    self.samples = os.listdir(self.vid_dir)
    self.num_samples = len(self.samples)

  def __len__(self):
    return self.num_samples #vid dir format "./embeddings"
  
  def __getitem__(self, idx):
    """ Override default getitem code to retrieve specific item in dataset
        visual - key for frame embeddings
        ques - key for list of all question embeddings
        ans  - key for list of all answer embeddings
        MATFile = { "visual":numpy_array
                    "ques"  :[numpy_array,...] 
                    "ans"  :[[token, token, token],...,[..]] 
                    }   
    """
    #ScipyIO COde to retreive vgg outputs as a list and question answer list
    idx = self.samples[idx][:-4]
    file_path = os.path.join(self.vid_dir, str(idx) + ".mat")
    print(file_path)
    data = scio.loadmat(file_path)
    video = torch.from_numpy(data["visual"][0].astype(np.float32))
    question = []
    for x in data["ques"][0]:
      if x.shape == (1,300):
        question.append(torch.from_numpy(data["ques"][0]))
        break
      question.append(torch.from_numpy(x))
    question = [x.squeeze(1) for x in question] #remove the batch size dimension introduced       
    answer = []
    if type(data["ans"][0][0]) is np.ndarray:
      for x in data["ans"][0]:
        answer.append(torch.squeeze(torch.from_numpy(x)))
    else:
      for x in data["ans"]:
        answer.append(torch.squeeze(torch.from_numpy(x)))
    return [video, question], answer


def train(model, epochs, batch_size, num_workers, learning_rate, vid_dir, vocab_path, output_size, vocab_length):
    fp = open(vocab_path, 'rb')
    vocabulary = list(pickle.load(fp))
    print(len(vocabulary))
    optimizer = optim.Adam(model.parameters(), lr=learning_rate)
    metric = nn.CrossEntropyLoss()
    dataset = VideoQADataset(vid_dir)
    loader = DataLoader(dataset, batch_size = batch_size, shuffle=True, num_workers = num_workers)
    decoder = Model.Decoder(output_size, vocab_length)
    for _ in range(epochs):
      for features, answer in loader: #features = [vid, que] answer = [ans]
          print('*'*50)
          for id in range(len(features[1])):
            labels = answer[id].squeeze(0)
            optimizer.zero_grad()
            frames = features[0].squeeze(0)
            ques = features[1][id].squeeze(0)
            vqa_rep = model.forward(frames, ques)
            #Use Decoder to generate answer tokens
            decoder.init_hidden()
            vqa_rep = vqa_rep.view(1,1, -1) #Make it LSTM Compatible
            predictions = []
            for _ in range(len(labels)):
              _, hidden = decoder(vqa_rep)
              ans_rep = hidden[0].view(vocab_length) #hidden state not
              predictions.append(ans_rep)
#           print(len(predictions), len(labels))
            predict = torch.stack(predictions, dim=0)
            #a = torch.argmax(predict, dim = 1)
            #print([vocabulary[x] for x in a])
            loss = metric(predict, labels)
            print(loss)
            loss.backward()
            optimizer.step()