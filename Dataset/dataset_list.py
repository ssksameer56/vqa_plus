#Code to generate a text file with list of GIF IDs in the folder. This file is used by DatasetCreator
#to get a list of GIF IDs to be processed
import os

VideoFolder = '/content/drive/My Drive/B Tech./B.Tech Project/Dataset/GIF_R'
files = os.listdir(VideoFolder)
new_files = [x[:-4] for x in files]

with open('videoid.txt', 'w') as f:
    for item in new_files:
        f.write("%s\n" % item)
