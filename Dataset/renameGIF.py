#Script to rename gifs with URLS in them. Renames them to IDs corresponding with QA.csv
import os, csv
# open and store the csv file
path = '/home/sameer/Desktop/Stuff/BTech Project/vqa_plus/Dataset/GIF/' #Original folder with tumblr names
tmpPath = '/home/sameer/Desktop/Stuff/BTech Project/vqa_plus/Dataset/GIF2/' #Folder should exist prior, contains gifs with IDs as names
csv_file = "./Sorted_data.csv"
IDs = {}
with open(csv_file) as csvfile:
    csv_reader = csv.reader(csvfile, delimiter = ',',  dialect=csv.excel_tab)
    # build dictionary with associated IDs
    for row in csv_reader:
        row[0] = row[0].rpartition('/')[-1]
        print(row[0], "\t", row[1])
        IDs[row[0]] = row[1]

# move files
#THIS MOVES AND NOT COPIES. ORIGINAL FOLDER WILL BE EMPTY AFTER THE SCRIPT RUNS
for oldname in os.listdir(path):
    # ignore files in path which aren't in the csv file
    if oldname in IDs:
        try:
            os.rename(os.path.join(path, oldname), os.path.join(tmpPath, IDs[oldname]))
            print("File "+ oldname + " Renamed to " + IDs[oldname])
        except Exception as e:
            print('File ' + oldname + ' could not be renamed to ' + IDs[oldname] + '!')
