import spacy
import csv
import numpy as np
import pickle
from nltk.tokenize import RegexpTokenizer
from progressbar import ProgressBar
pbar = ProgressBar()

vocab_path = '/content/drive/My Drive/Dataset/Vocabulary'
csv_path = '/content/drive/My Drive/Dataset/QA.csv'
class TextProcess():
    def __init__(self):
        self.nlp = spacy.load("en_core_web_sm")
        self.nltk_tokenizer = RegexpTokenizer(r'\w+')
        self.all_tokens = set()
        self.all_tokens = {'<EOS>', '<SOS>'}
        
    def tokenize(self, sentence):
        token_list = set()
        doc = self.nlp(sentence)
        for token in doc:
            token_list.add(token.text.lower())
        return token_list
    def addElementsToSet(self, subset):
        for x in subset:
            self.all_tokens.add(x)
    def nltk_tokenize(self, sentence):
        return self.nltk_tokenizer.tokenize(sentence.lower())

t = TextProcess()
with open(csv_path) as csvfile:
    csv_reader = csv.reader(csvfile, delimiter = ',')
    for row in pbar(csv_reader):
        tokens_ans = t.nltk_tokenize(row[2])
        #print(tokens_ans)
        t.addElementsToSet(tokens_ans)
        #print(t.all_tokens)

with open(vocab_path, 'wb+') as f:
    pickle.dump(t.all_tokens, f)

print(len(t.all_tokens))

def readFromFile():
    with open(vocab_path, 'rb') as fp:
       vocabList = pickle.load(fp)
    reurn vocabList
vocabList  = list(readFromFile(path))
print(vocabList)
